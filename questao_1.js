/*Implemente um algoritmo que pegue duas matrizes (array de arrays) e
realize sua multiplicação. Lembrando que para realizar a multiplicação
dessas matrizes o número de colunas da primeira matriz tem que ser
igual ao número de linhas da segunda matriz. (2x2)*/

function multiply(a, b) {
    var aNumLinha = a.length, aNumCols = a[0].length,
        bNumLinha = b.length, bNumCols = b[0].length,
        matriz = new Array(aNumLinha);  
    for (var r = 0; r < aNumLinha; ++r) {
      matriz[r] = new Array(bNumCols); 
      for (var c = 0; c < bNumCols; ++c) {
        matriz[r][c] = 0;            
        for (var i = 0; i < aNumCols; ++i) {
          matriz[r][c] += a[r][i] * b[i][c];
        }
      }
    }
    return matriz;
  }
  
  function display(m) {
    for (var r = 0; r < m.length; ++r) {
      document.write('  '+m[r].join(' ')+'<br />');
    }
  }
  
  var a = [[2,-1], [2,0]],
  
      b = [[2,3], [-2,1]] ;

  document.write('Matrix a:<br />');
  display(a);
  document.write('Matrix b:<br />');
  display(b);
  document.write('Matriz a * Matriz b = <br />');
  display(multiply(a, b));